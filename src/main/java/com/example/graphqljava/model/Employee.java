package com.example.graphqljava.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Employee {
    int id;
    String name;
    double salary;
}

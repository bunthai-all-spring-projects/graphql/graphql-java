package com.example.graphqljava.service.impl;

import com.example.graphqljava.model.Employee;
import com.example.graphqljava.service.EmployeeService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    List<Employee> employees;

    @PostConstruct
    void loadData() {
        employees = new ArrayList<>() {{
            add(Employee.builder().id(1).name("jam").salary(1000.00).build());
            add(Employee.builder().id(2).name("Sam").salary(500.00).build());
        }};
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employees;
    }

    @Override
    public Employee findEmployeeById(int id) {
        return employees.stream().filter(e -> e.getId() == id).findFirst().orElse(null);
    }

    @Override
    public Employee addEmployee(String name, double salary) {
        int generateId = employees.size() + 1;
        Employee employee = Employee.builder().id(generateId).name(name).salary(salary).build();
        employees.add(employee);
        return employee;
    }
}

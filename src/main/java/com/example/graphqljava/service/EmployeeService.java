package com.example.graphqljava.service;

import com.example.graphqljava.model.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> getAllEmployees();
    Employee findEmployeeById(int id);

    Employee addEmployee(String name, double salary);
}

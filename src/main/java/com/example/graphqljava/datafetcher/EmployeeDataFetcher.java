package com.example.graphqljava.datafetcher;

import com.example.graphqljava.model.Employee;
import com.example.graphqljava.service.EmployeeService;
import graphql.schema.DataFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeDataFetcher {

    @Autowired
    EmployeeService employeeService;


    public DataFetcher<Employee> findEmployeeById() {

        return environment -> {
            int id = environment.getArgument("id");
            Employee employee = employeeService.findEmployeeById(id);
            return  employee;
        };
    }

    public DataFetcher<Employee> addEmployee() {

        return environment -> {
            String name = environment.getArgument("name");
            double salary = environment.getArgument("salary");
            Employee employee = employeeService.addEmployee(name, salary);
            return  employee;
        };
    }

}

package com.example.graphqljava.controller;

import com.example.graphqljava.datafetcher.EmployeeDataFetcher;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.StaticDataFetcher;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
public class GraphQLController {

    GraphQL graphQL;


    @Autowired
    EmployeeDataFetcher dataFetcher;

    @PostConstruct
    void loadSchema() {
        String schema = """
                
                    type Query {
                        hello: String
                        findEmployeeById(id: Int): Employee
                    }
                    
                    type Mutation {
                        addEmployee(name: String, salary: Float): Employee
                    }
                    
                    type Employee {
                        id: Int,
                        name: String,
                        salary:  Float
                    }
                
                """;

        SchemaParser schemaParser = new SchemaParser();

        // Prepare expose schema
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        // Mapping Mutation/Query schema function with runtime DAO, Service, Function
        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder.dataFetcher("hello", new StaticDataFetcher("hello")))
                .type("Query", builder -> builder.dataFetcher("findEmployeeById", dataFetcher.findEmployeeById()))
                .type("Mutation", builder -> builder.dataFetcher("addEmployee", dataFetcher.addEmployee()))
                .build();

        // Bind schema and runtime DAO, Service, Function
        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        graphQL = GraphQL.newGraphQL(graphQLSchema).build();

    }

    @PostMapping("/graphql")
    public Object graphql(@RequestBody String body) {
        var executionResult = graphQL.execute(body);
        return executionResult.getData();
    }

}

Query:
```graphql
query {
    findEmployeeById(id: 3) {
        id
        name
    }
}
```


Muation:

```graphql
mutation {

	addEmployee(name: "Key", salary: 2000.00) {
		id
		name
		salary
	}

}
```
